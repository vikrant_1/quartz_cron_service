﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;

namespace QuartzApp
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsService();
        }

        static void RunAsService()
        {
            ServiceBase[] servicesToRun;
            servicesToRun = new ServiceBase[]{ new VikrantService() };
            ServiceBase.Run(servicesToRun);
            
        }
    }
}
