﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using log4net;
using Quartz.Impl;
using log4net.Config;
using System.Data.SqlClient;

                                 // Equivalent to Job class

namespace QuartzApp
{
    partial class VikrantService : ServiceBase
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected string VikrantJob = "VikrantRunScheduleJob";
        protected string VikrantGroup = "VikrantRunScheduleGroup";
        protected string VikrantTrigger = "VikrantRunScheduleTrigger";
        protected string VikrantScheduleStartedMsg = "Vikrant's schedule hass been started";
        protected string VikrantScheduleStoppedMsg = "VIkrant's Schedule has been Stopped";
        protected static string  VikrantJobStartedMsg = "Vikrant's job has been started";
        protected static string VikrantJobStoppedMsg = "Vikrant's job has been stopped";

        public VikrantService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
          
            log4net.Config.XmlConfigurator.Configure();
            Log.Info(VikrantScheduleStartedMsg);
            RunApp();
        }

        protected override void OnStop()
        {
            Log.Info(VikrantScheduleStoppedMsg);
        }

        public void RunApp()
        {
              
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            IScheduler sched = schedFact.GetScheduler();
            sched.Start();

            var job = JobBuilder.Create<ExecutingJobs>()
                        .WithIdentity(VikrantJob , VikrantGroup)
                        .Build();
            try
            {
                
                var config = Common.GetConfiguration();

                
                var timeToRunInDays = config.ScheduleIntervalInDays * 24 * 60;
                var timeToRunInHours = config.ScheduleIntervalInHours * 60;
                var timeToRunInMinutes = config.ScheduleIntervalInMinutes;
                var timeToRunInSeconds = config.ScheduleIntervalInSeconds;
                var scheduleTime = timeToRunInDays + timeToRunInHours + timeToRunInMinutes + timeToRunInSeconds;
                var startAtHours = config.ScheduleStartAtHours;
                var startAtMinutes = config.ScheduleStartAtMinutes;
                var startAtSeconds = config.ScheduleStartAtSeconds;

             
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity(VikrantTrigger, VikrantGroup)
                    .StartAt(DateTime.Now.Date.AddHours(startAtHours)
                                              .AddMinutes(startAtMinutes)
                                              .AddSeconds(startAtSeconds))

                    .WithSimpleSchedule(x => x.WithIntervalInSeconds(scheduleTime).RepeatForever())
                    .Build();

         
                sched.ScheduleJob(job, trigger);
            }
            catch (SchedulerException se)
            {
                Log.Error(se.Message);
            }
        }


       
        public class ExecutingJobs : IJob
        {
            SqlConnection con1, con2;

            void IJob.Execute(IJobExecutionContext context)
            {
                Log.Info(VikrantJobStartedMsg);
                ExecutingTasks();
                Log.InfoFormat(VikrantJobStoppedMsg);
            }

             // Your main code exist in this section
            private void ExecutingTasks()
            {
                con1 = new SqlConnection("Data Source=DESKTOP-7MRDTE0\\SQLEXPRESS2014;Initial Catalog=DB1;User ID=sa;Password=Vikrant@SQL");
                con1.Open();
                con2 = new SqlConnection("Data Source=DESKTOP-7MRDTE0\\SQLEXPRESS2014;Initial Catalog=DB2;User ID=sa;Password=Vikrant@SQL");
                con2.Open();

                try
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter da = new SqlDataAdapter("select * from TBL1", con1);
                    SqlCommandBuilder cb = new SqlCommandBuilder(da);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    int i;

                    foreach (DataRow dr in dt.Rows )
                    {
                        cmd = new SqlCommand("Insert into TBL2 Values('" + dr[0].ToString() + "','" + dr[1].ToString() + "','" + dr[2].ToString() + "')", con2);
                        i = cmd.ExecuteNonQuery();
                        dr[3] = true;
                    }
                    da.Update(dt);
                    cmd.Dispose();
                    da.Dispose();
                    dt.Dispose();

                }
                catch (Exception ex)
                {
                    Console.WriteLine("{0}: Exception caught", ex.Message);
                }

                try
                {
                    var now = DateTime.Now.ToString("yyyyMMdd-HHMMss");
                    Log.Info(String.Format("Executed job at {0}", now));
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                }
            }
        }
    }
}
